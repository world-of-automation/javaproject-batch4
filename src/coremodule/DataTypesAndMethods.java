package coremodule;

public class DataTypesAndMethods {
    static String name = "my name is sourav";
    int number = 1000;

    public DataTypesAndMethods() {

    }

    public static void main(String[] args) {
        DataTypesAndMethods dataTypesAndMethods = new DataTypesAndMethods();
        String returnedData = dataTypesAndMethods.getMyName();
        System.out.println(returnedData);
        dataTypesAndMethods.printMyName();
        System.out.println(name);
    }

    // void
    public void printMyName() {
        System.out.println("adnan");
    }

    //return type
    public String getMyName() {
        String name = "my name is opu";
        return name;
    }

}
