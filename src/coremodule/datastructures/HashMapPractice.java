package coremodule.datastructures;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapPractice {

    public static void main(String[] args) {
        HashMap<String, String> states = new HashMap<>();
        states.put("rego park", "11374");
        states.put("ozone park", "11233");
        states.put("prospect park", "234234");

        System.out.println(states.get("rego park"));
        System.out.println(states.size());

        ArrayList<String> keysAsArray = new ArrayList<>(states.keySet());
        System.out.println(keysAsArray);

        for (int i = 0; i < states.size(); i++) {
            System.out.println(states.get(keysAsArray.get(i)));
        }

    }
}
