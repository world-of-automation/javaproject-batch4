package coremodule.datastructures;

public class Arrays2 {

    public static void main(String[] args) {
        String[] names = new String[4];

        names[0] = "Opu";
        names[1] = "Adnan";

        System.out.println(names[1]);
        System.out.println(names[3]);

        System.out.println(names.length);

    }
}
