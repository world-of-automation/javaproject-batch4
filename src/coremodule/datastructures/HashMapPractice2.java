package coremodule.datastructures;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapPractice2 {


    public static void main(String[] args) {
        ArrayList<String> cityAsArrayList = new ArrayList<>();
        cityAsArrayList.add("rego park");
        cityAsArrayList.add("ozone park");
        cityAsArrayList.add("prospect park");

        ArrayList<String> zipcodeAsArrayList = new ArrayList<>();
        zipcodeAsArrayList.add("11374");
        zipcodeAsArrayList.add("11322");
        zipcodeAsArrayList.add("34223");

        HashMap<String, ArrayList<String>> statesMap = new HashMap();

        statesMap.put("city", cityAsArrayList);
        statesMap.put("zipcode", zipcodeAsArrayList);

        System.out.println(statesMap);

        ArrayList<String> keys = new ArrayList<>(statesMap.keySet());

        for (int i = 0; i < statesMap.size(); i++) {
            System.out.println(statesMap.get(keys.get(i)));
        }

        System.out.println("******");

        for (int i = 0; i < statesMap.size(); i++) {
            for (int j = 0; j < statesMap.get(keys.get(i)).size(); j++)
                System.out.println(statesMap.get(keys.get(i)).get(j));
        }

    }
}
