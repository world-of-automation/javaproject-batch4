package coremodule.datastructures;

import java.util.HashMap;
import java.util.Hashtable;

public class HashTablePractice {

    public static void main(String[] args) {


        Hashtable<String, Object> hashtable = new Hashtable<>();

        hashtable.put("name", "adnan");
        hashtable.put("age", 23);
        hashtable.put("salary", 125000);
        //hashtable.put("ef",null);

        //System.out.println(hashtable);


        HashMap<String, Object> map = new HashMap<>();
        map.put(null, "something");
        map.put("ss", null);
        map.put(null, "everything");
        System.out.println(map);
    }
}
