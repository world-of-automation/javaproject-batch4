package coremodule.datastructures;

import java.util.LinkedList;

public class LinkedListPractice {

    public static void main(String[] args) {

        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("rahman");
        linkedList.add("sourav");
        linkedList.addLast("siam");
        linkedList.addFirst("adnan");
        linkedList.addLast("opu");

        System.out.println(linkedList);

        System.out.println("***********");

        linkedList.removeFirst();
        linkedList.removeLast();
        System.out.println(linkedList);
    }
}
