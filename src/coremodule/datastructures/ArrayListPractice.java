package coremodule.datastructures;

import java.util.ArrayList;

public class ArrayListPractice {

    public static void main(String[] args) {

        ArrayList arrayList = new ArrayList();
        arrayList.add("opu");
        arrayList.add("adnan");
        arrayList.add("sourav");

        System.out.println(arrayList.size());

        System.out.println(arrayList.get(0));


        for (int i = arrayList.size(); i > 0; i--) {
            System.out.println(arrayList.get(i - 1));
        }

        System.out.println("*********");

        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

    }
}
