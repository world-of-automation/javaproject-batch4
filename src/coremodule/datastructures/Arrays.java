package coremodule.datastructures;

public class Arrays {

    static String[] names = {"opu", "sourav", "adnan", "rahman"};

    public static void main(String[] args) throws Exception {
        String name = "Opu";
        System.out.println(name);
        //print individual
        System.out.println(names[2]);
        //print the length
        System.out.println(names.length);
        //print all the names
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }
        // catch the exception, perform something & not fail
        printMyArrays();

        // throw the exception & fail
        printMyArrays2();

        // catch the exception, perform something & fail
        printMyArrays3();
    }

    public static void printMyArrays3() {
        try {
            System.out.println(names[4]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println(" exception in the loop, please correct the index");

        }
    }

    public static void printMyArrays2() throws Exception {
        System.out.println(names[5]);
    }

    public static void printMyArrays() {
        try {
            System.out.println(names[5]);
        } catch (ArrayIndexOutOfBoundsException ee) {
            System.out.println("failed");
        }
    }
}
