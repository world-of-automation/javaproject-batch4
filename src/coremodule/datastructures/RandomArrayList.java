package coremodule.datastructures;

import java.util.ArrayList;
import java.util.Random;

public class RandomArrayList {

    public static void main(String[] args) {
        Random random = new Random();
        System.out.println(random.nextInt(10000));
        // object arraylist
        ArrayList arrayList = new ArrayList();

        for (int i = 0; i < 10; i++) {
            arrayList.add(random.nextInt(10000));
        }
        System.out.println(arrayList.size());
        System.out.println(arrayList);
        arrayList.add("opu");
        System.out.println(arrayList);

        //declared data typed arraylist
        ArrayList<String> names = new ArrayList<>();
        names.add("opu");
        // names.add(123);
    }
}
