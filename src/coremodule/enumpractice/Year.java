package coremodule.enumpractice;

public class Year {

   /* Months months;

    public Year(Months months){
        this.months=months;
    }*/

    public static void main(String[] args) {
        // Year year = new Year(Months.January);
        Year year = new Year();
        year.weatherOfTheYear(Months.December);
        year.weatherOfTheYear(Months.January);
    }

    public void weatherOfTheYear(Months months) {
        switch (months) {
            case January:
                System.out.println("colddd");
                break;
            case February:
            case March:
            case April:
            case May:
                System.out.println("its getting better");
                break;
            case July:
            case June:
                System.out.println("its summer time");
                break;
            default:
                System.out.println("not interested");
                break;
        }
    }


}
