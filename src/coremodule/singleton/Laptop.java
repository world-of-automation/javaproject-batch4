package coremodule.singleton;

public class Laptop {

    private static Laptop laptop = null;
    int year;

    private Laptop(int year) {
        this.year = year;
    }


    public static Laptop getInstance(int year) {
        if (laptop == null) {
            laptop = new Laptop(year);
        }
        return laptop;
    }

    public void printLaptopYear() {
        System.out.println(year);
    }
}
