package coremodule.singleton;

public class LaptopExec {

    public static void main(String[] args) {
        Laptop laptop = Laptop.getInstance(2019);
        laptop.printLaptopYear();


        Laptop laptop2 = Laptop.getInstance(2020);
        laptop2.printLaptopYear();

    }
}
