package coremodule.loopsandifelse;

public class IfElse {

    public static void main(String[] args) {
        int x = 1;

        if (x > 2) {
            System.out.println("x is greatter than 2");
        } else if (x > 0) {
            System.out.println("x is greatter than 0");
        }

        int number = 10;

        if (number / 2 == 5 && number / 10 == 1) {
            System.out.println("its equal to 5 & its equal to 1");
        }


        if (number / 2 == 5 || number / 10 == 2) {
            System.out.println("its equal to 5 & its equal to 1");
        }

    }
}
