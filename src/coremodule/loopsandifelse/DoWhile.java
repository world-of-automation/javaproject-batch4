package coremodule.loopsandifelse;

public class DoWhile {

    public static void main(String[] args) {
        int x = 0;
        do {
            System.out.println("while loop " + x);
            x++;
        }
        while (x < 10);
    }
}
