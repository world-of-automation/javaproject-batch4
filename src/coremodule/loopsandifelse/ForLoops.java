package coremodule.loopsandifelse;

public class ForLoops {

    public static void main(String[] args) {

        //starting point; end point; increment/decrement
        for (int i = 0; i < 10; i++) {
            System.out.println("This is loop " + i);
            if (i > 4) {
                break;
            }
        }

        for (int i = 0; i < 10; i++) {
            System.out.println("selenium" + i);
            for (int x = 0; x < 2; x++) {
                System.out.println("java");
            }
        }
    }
}
