package coremodule.loopsandifelse;

public class WhileLoops {

    public static void main(String[] args) {
        int x = 0;
        while (x < 10) {
            System.out.println("while loop is running " + x);
            if (x > 6) {
                break;
            }
            x++;
        }


    }

}
