package coremodule.accessspecifier;

public class TestClass {
    public String location = "bronx";
    protected String model = "asus";
    //global variable
    String name = "opu";
    private String year = "2020";
    // private -- same class only
    // public -- everywhere
    // default -- same class, different class in same package
    // protected -- same class, different class in same package,
    // inner class in different class in same package

    private void metho1() {
        //local variable-->
        String dob = "23124";
        System.out.println(name);
        System.out.println(year);
    }
}
