package coremodule.accessspecifier;

public class ExecutionClass {
    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        String nameFromTestClass = testClass.name;
        System.out.println(nameFromTestClass);

    }


    public class innerClass {
        public void innerMethod() {
            TestClass testClass = new TestClass();
            String mm = testClass.model;
            System.out.println(mm);
        }
    }
}
