package coremodule.database;

import java.sql.*;
import java.util.ArrayList;

public class SQLConnect_3 {

    public static void main(String[] args) throws SQLException {
        String query = "select * from student;";
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        ResultSet table = statement.executeQuery(query);

        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> location = new ArrayList<>();

        while (table.next()) {
            id.add(table.getInt("id"));
            names.add(table.getString("name"));
            location.add(table.getString("location"));
        }

        System.out.println(id);
        System.out.println(names);
        System.out.println(location);

        cleanUpDatabase(statement, connection);
    }

    public static Connection getConnection() throws SQLException {
        String username = "root";
        String password = "root1234";
        String databaseName = "batch4";
        String url = "jdbc:mysql://localhost:3306/" + databaseName;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

    public static void cleanUpDatabase(Statement statement, Connection connection) {
        try {
            statement.close();
            connection.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
