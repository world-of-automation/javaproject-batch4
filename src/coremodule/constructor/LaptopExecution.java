package coremodule.constructor;

import coremodule.accessspecifier.TestClass;

public class LaptopExecution {

    public static void main(String[] args) {
        Laptop laptop = new Laptop("Apple");
        laptop.printMyCompany();
        System.out.println(Laptop.model);
        System.out.println(laptop.year);
        Laptop laptop2 = new Laptop("Asus", 2019);
        laptop2.printMyCompany();
        laptop2.printMyYear();
        laptop2.printMyCompanyAndYear();
        TestClass testClass = new TestClass();
        TestConstructor testConstructor = new TestConstructor();
        testConstructor.printme();
        Laptop.getMyModel();
    }
}
