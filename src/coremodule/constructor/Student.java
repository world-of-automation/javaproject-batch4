package coremodule.constructor;

public class Student {

    static {
        System.out.println("static block is running");
    }

    String name;

    public Student() {
        System.out.println("constructor is running");
    }

    public void printMyName() {
        System.out.println(name);
    }
}
