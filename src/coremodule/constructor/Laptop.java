package coremodule.constructor;

public class Laptop {

    public static String model = "macbook pro";
    String company;
    int year = 2017;

    public Laptop(String company, int year) {
        this.company = company;
        this.year = year;
    }

    public Laptop() {

    }

    public Laptop(String company) {
        this.company = company;
    }

    public static void getMyModel() {
        System.out.println(model);
    }

    public void printMyCompany() {
        System.out.println(company);
    }

    public void printMyYear() {
        System.out.println(year);
        System.out.println(model);
    }

    public void printMyCompanyAndYear() {
        System.out.println(company);
        System.out.println(year);
    }
}
