package coremodule.oop;

public class PolymorphismCalculator {

    // polymorphism --> having many forms

    public static void main(String[] args) {
        PolymorphismCalculator polymorphismCalculator = new PolymorphismCalculator();
        polymorphismCalculator.addition(10, 15, 20);
        polymorphismCalculator.addition(10, 15);

    }

    // method overloading -->same method name, different params, same class
    // known as static polymorphism/ compile time polymorphism

    public void addition(int x, int y, int z) {
        System.out.println(x + y + z);
    }

    public void addition(int x, int y) {
        System.out.println(x + y);
    }


    // method overriding -->same method name, same params, different class
    // known as dynamic polymorphism/ run time polymorphism
    // see examples at Abstraction
}
