package coremodule.oop.encapsulation;

public class StudentExec {
    public static void main(String[] args) {
        Student student = new Student();
        student.setCollege("SOmething");
        String collegeOfStudent = student.getCollege();
        System.out.println(collegeOfStudent);
    }
}
