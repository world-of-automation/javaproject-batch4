package coremodule.oop.abstraction;

public class TestCar {

    public static void main(String[] args) {
        RunableCar runableCar = new RunableCar();
        runableCar.move();
        runableCar.start();
        runableCar.color();

        FutureCar futureCar = new FutureCar();
        futureCar.color();
    }
}
