package coremodule.oop.abstraction;


public class RunableCar extends Car implements Vehicle, Vehicle2, Vehicle3 {

    // Interface --> Implements (I-I)
    // Class/ Abstract class --> Extends
    // extend first, then implement
    // implement multiple interface
    // can't extend more than one class/abstract class


    @Override
    public void move() {
        System.out.println("car can move");
    }

    @Override
    public void start() {
        System.out.println("car can start");
    }

    @Override
    public void stop() {
        System.out.println("car can stop");
    }

    @Override
    public String name() {
        return "bmw";
    }

    @Override
    public void wheels() {
        System.out.println("car has 4 wheels");
    }
}
