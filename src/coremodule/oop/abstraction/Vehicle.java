package coremodule.oop.abstraction;

public interface Vehicle extends Vehicle3, Vehicle2 {
    // interface is just an concept
    // interface can extend multiple interfaces
    // interface can't implement

    public void move();

    public void start();

    public void stop();

    public String name();


    // can't have method with body
    // can't have constructor
    // can have return/void type method names--> not body


}
